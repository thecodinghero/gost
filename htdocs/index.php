<?php
    require_once('gost.php');
    try {
        $g  = new Gost('./base', '../conf');
        $c = $g->config();
        $db = new DB($c);

        // load some modules and init the state
        $h = $g->module('home');
        $t = $g->module('test');

        // give the home module access to the database
        $h->database($db);

        // run the modules
        $h->run();
        $t->run();

        // set the display
        $d = $g->display();

        // load up the tiles
        $d->load('header');
        $d->load('home');
        $d->load('footer');

        // load up the templates
        $d->show('main', 'header');
        $d->show('home', 'body');
        $d->show('main', 'footer');

        // render it
        $d->render();
        $g->dump($g);
    } catch (Exception $e) {
        die($e);
    }
?>