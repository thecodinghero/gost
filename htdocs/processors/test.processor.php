<?php
    class TestProcessor extends Processor {
        public function run() {
            if ($this->state->get('form', 'value')) {
                $this->state->data('text', $this->state->get('form', 'value'));
            } else {
                $this->state->data('text', 'Error');
            }

            $this->load('text');
            $this->show('form', 'text');
            $this->render(); 

            return(true);
        }
    }
?>