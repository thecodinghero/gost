<?php
    class HomeModule extends Module {
        public function run() {
            // get data for the tiles
            $this->db->connect();
            $this->db->load('test');
            $users = $this->db->query('test')->getMysqlUsers();
            if ($users) {
                $this->state->data('users', $users);
            }
            $this->db->close();
        }
    }
?>