<?php
    class ProcessorModule extends Module {
        public function process() {
            foreach ($_GET as $key => $value) {
                $this->state->form($key, $value);
            }

            foreach ($_POST as $key => $value) {
                $this->state->form($key, $value);
            }

            foreach ($_FILES as $key => $value) {
                $this->state->form($key, $value);
            }

            if ($name = $this->state->get('form', 'method')) {
                if(include_once("{$this->config()->get('path', 'processors')}/{$name}.processor.php")) {
                    $class  = "{$name}processor";
                    if ($processor = new $class($this->state, $this->db)) {
                        return($processor->run());
                    } else {
                        throw new Exception("could not load {$name}.processor.php");
                    }
                } else {
                    throw new Exception('could not find processor method');
                }
            } else {
                throw new Exception('processor method not defined');
            }
        }

        public function tile() {
            return($this->state->get('display'));
        }

        public function template() {
            return($this->state->get('template'));
        }
    }
?>