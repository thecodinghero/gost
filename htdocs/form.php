<?php
    require_once('gost.php');
    try {
        $g  = new Gost('./base', '../conf');
        $c  = $g->config();
        $db = new DB($c);

        $form = $g->module('processor', $g->config());
        $form->database($db);

        $form->process();
        $g->dump($form);
    } catch (Exception $e) {
        die($e);
    }
?>
