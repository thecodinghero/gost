<?php
    class Gost {
        private $config;
        private $state;

        public function __construct($lib=null, $config=null) {
            if (!empty($lib) && !empty($config)) {
                if (require_once("{$lib}/config.class.php")) {
                    $this->config = new Config($config);
                    if (require_once("{$this->config->get('path', 'lib')}/state.object.php")) {
                        $this->state = new State($this->config);
                        return($this);
                    } else {
                        throw new Exception('gost could not initialize state object');
                    }
                } else {
                    throw new Exception('gost could not load config class');
                }
            } else {
                throw new Exception('missing params in gost construct');
            }
        }

        public function config() {
            return($this->config);
        }

        public function state() {
            return($this->state);
        }

        public function module($name=null) {
            if (!empty($name)) {
                if (include_once("{$this->config->get('path', 'modules')}/{$name}.module.php")) {
                    $class = "{$name}module";
                } else {
                    $class = 'module';
                }

                if ($module = new $class($name, $this->state)) {
                    return($module);
                } else {
                    throw new Exception("could not load {$name}.module.php");
                }
            } else {
                throw new Exception('module name not defined');
            }
        }

        public function display() {
            return(new Display($this->state));
        }

        public function dump($var=null) {
            print("<pre style=\"{$this->config()->get('debug_style')}\">");
            print_r($var);
            print('</pre>');
        }
    }
?>