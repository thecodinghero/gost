<?php
    class Query {
        protected $con;


        public function __construct(&$con=null) {
            if (!empty($con)) {
                $this->con = &$con;
                return($this);
            } else {
                throw new Exception('connection not defined');
            }
        }

        protected function query($a=null) {
            if (!empty($a)) {
                for ($i = 0; $i < count($a); $i++) {
                    $p[] = $a[$i];
                }

                $s = call_user_func_array('sprintf', $p);
                $r = mysql_query($s, $this->con);

                if (stristr($s, 'SELECT')) {
                    if (is_resource($r)) {
                        $d = array();
                        while ($t = mysql_fetch_assoc($r)) {
                            $d[] = $t;
                        }

                        return($d);
                    } else {
                        throw new Exception('invalid result resource');
                    }
                } else {
                    return($r);
                }
            } else {
                throw new Exception('emtpy query string');
            }
        }

        protected function prepare() {
            $a = func_get_args();
            if (count($a) > 0) {
                for ($i = 0; $i < count($a); $i++) {
                    if ($i > 0) {
                        $r[] = mysql_real_escape_string($a[$i]);
                    } else {
                        $r[] = $a[$i];
                    }
                }

                return($r);
            } else {
                throw new Exception('could not prepare statement');
            }

        }
    }
?>