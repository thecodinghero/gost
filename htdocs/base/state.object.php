<?php
    class State extends Object {
        private $config;


        public function __construct(&$config) {
            if (!empty($config)) {
                unset($this->state);
                $this->config = &$config;
                $this->set('request');
                $this->set('form');
                $this->set('data');
                $this->set('tiles');
                $this->init();
                return($this);
            } else {
                throw new Exception('missing config in state object');
            }
        }

        public function config() {
            return($this->config);
        }

        public function data($name=false, $data=false) {
            if ($name) {
                $this->set('data', $name, $data);
            } else {
                throw new Exception('missing parameters setting data in state');
            }
        }

        public function form($name=false, $data=null) {
            if ($name) {
                $this->set('form', $name, $data);
            } else {
                throw new Exception('missing parameters setting form data in state');
            }
        }

        private function init() {
            foreach ($_REQUEST as $key => $value) {
                $this->set('request', $key, $value);
            }
        }
    }
?>
