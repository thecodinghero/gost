<?php
    /**
     * Object
     * 
     * This is the base class of all core GOST objects.
     */
    class Object {
        protected $state;
        protected $vars = array();


        /**
         * __construct
         * 
         * This... is the construct.  It's our loading program.
         * Right now, we're inside a computer program.
         * 
         * @param   object  $state  a reference to the state object
         * @return  object  $this
         */
        public function __construct(&$state) {
            if (!empty($state)) {
                $this->state = &$state;
                return($this);
            } else {
                throw new Exception('missing state in base object');
            }
        }

        /**
         * state
         *
         * Public access method to return the state object.
         * 
         * @return  object  $state  a state object
         */
        public function state() {
            return($this->state);
        }

        /**
         * get
         * 
         * Public access method to return the requested data from vars.
         * 
         * @param   string  $name   name of an array, key in an array, or name of a value
         * @param   string  $key    key in an array or name of a value
         * @param   string  $value  name of a value
         * @return  mixed           either a specified hash, array, or value
         */
        public function get($name=null, $key=null, $value=null) {
            switch (func_num_args()) {
                case 1:
                    return(isset($this->vars[$name]) ? $this->vars[$name] : null);
                    break;
                case 2:
                    return(isset($this->vars[$name][$key]) ? $this->vars[$name][$key] : null);
                    break;
                case 3:
                    return(isset($this->vars[$name][$key][$value]) ? $this->vars[$name][$key][$value] : null);
                    break;
            }
        }

        /**
         * set
         * 
         * Public access method to save the specified data in vars.
         * 
         * @param  string  $name   name of an array, key in an array, or name of a value
         * @param  string  $key    key in an array or name of a value
         * @param  string  $value  name of a value
         */
        public function set($name=false, $key=false, $value=false) {
            if ($name) {
                if ($key !== false) {
                    if ($value !== false) {
                        $this->vars[$name][$key] = $value;
                    } else {
                        $this->vars[$name][$key] = array();
                    }
                } elseif ($value !== false) {
                    $this->vars[$name] = $value;
                } else {
                    $this->vars[$name] = array();
                }
            }
        }

        /**
         * insert
         * 
         * Protected method to insert an element into a named array in vars.
         * 
         * @param  string  $name   name of an array
         * @param  string  $value  name of a value
         */
        protected function insert($name=false, $value=null) {
            if ($name) {
                $this->vars[$name][] = $value;
            }
        }

        /**
         * delete
         * 
         * Protected method to remove an element, or value from vars.
         * 
         * @param  string  $name   name of an array
         * @param  string  $value  name of a value
         */
        protected function delete($name=false, $key=false) {
            if ($name) {
                if ($key !== false) {
                    unset($this->vars[$name][$key]);
                } else {
                    unset($this->vars[$name]);
                }
            }
        }

        /**
         * config
         * 
         * Protected method to return the config object.
         * 
         * @return  object  Config object
         */
        protected function config() {
            return($this->state->config());
        }
    }
?>
