<?php
    class DB {
        private $host;
        private $user;
        private $pass;
        private $name;
        private $con;

        private $conf;

        protected $library = array();


        public function __construct(&$config=null) {
            if (!empty($config)) {
                $this->conf = &$config;

                $this->host = $config->db['host'];
                $this->user = $config->db['user'];
                $this->pass = $config->db['pass'];
                $this->name = $config->db['name'];

                return($this);
            } else {
                throw new Exception('missing config in db object');
            }
        }

        public function connect() {
            $this->con = mysql_connect($this->host, $this->user, $this->pass, false);
            if ($this->con) {
                if (mysql_select_db($this->name, $this->con)) {
                    if ($this->conf->get('log_db_connection') === true) {
                        print("connected to database {$this->name} on {$this->user}@{$this->host}");
                    }
                } else {
                    throw new Exception("error selecting database {$this->name}");
                }
            } else {
                throw new Exception("error connecting to database {$this->user}@{$this->host}");
            }
        }

        public function close() {
            mysql_close($this->con);
        }

        public function get() {
            return($this->con);
        }

        public function load($name=null) {
            if (!empty($name)) {
                if (include_once("{$this->conf->get('path', 'queries')}/{$name}.query.php")) {
                    $class = "{$name}query";
                    $this->library[$name] = new $class($this->con);
                } else {
                    throw new Exception("could not load query library {$this->conf->get('path', 'queries')}/{$object}.query.php");
                }
            } else {
                throw new Exception('no query library defined');
            }
        }

        public function query($object=null) {
            if (!empty($object)) {
                return($this->library[$object]);
            } else {
                throw new Exception('no query defined');
            }
        }
    }
?>