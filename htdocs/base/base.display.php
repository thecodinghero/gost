<?php
    class Display extends Object {
        protected $output;


        public function __construct(&$state=null) {
            if (!empty($state)) {
                $this->state = &$state;
                return($this);
            } else {
                throw new Exception('missing state in display object');
            }
        }

        public function load($name=null, $tile=null) {
            if (empty($tile)) {
                $tile = $name;
            }

            if (!empty($name)) {
                ob_start();
                if (include_once("{$this->config()->get('path', 'tiles')}/{$name}.tile.php")) {
                    $o = ob_get_contents();
                    ob_end_clean();
                    $this->state->set('tiles', $tile, $o);
                } else {
                    ob_end_clean();
                    throw new Exception("could not load {$name}.tile.php");
                }
            } else {
                throw new Exception('tile name not defined');
            }
        }

        public function tile($name=null) {
            if (!empty($name)) {
                return($this->state->get('tiles', $name));
            } else {
                throw new Exception('tile not defined');
            }
        }

        public function show($name=null, $type=null) {
            if (!empty($name) && !empty($type)) {
                ob_start();
                if (include_once("{$this->config()->get('path', 'templates')}/{$name}.{$type}.template.php")) {
                    $o = ob_get_contents();
                    ob_end_clean();
                    $this->output .= $o;
                } else {
                    ob_end_clean();
                    throw new Exception("could not load {$name}.{$type}.template.php");
                }
            } else {
                throw new Exception('template name or type not defined');
            } 
        }

        public function render() {
            print($this->output);
            return(true);
        }
    }
?>