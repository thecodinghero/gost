<?php
    class Module extends Object {
        private   $name;
        protected $db;

        
        public function __construct($name=null, &$state) {
            parent::__construct($state);
            $this->state->insert('module', $name);
        }

        public function database(&$db=null) {
            $this->db = &$db;
        }
    }
?>