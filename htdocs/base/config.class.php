<?php
    class Config {
        private $base;


        public function __construct($conf=null) {
            if (!empty($conf)) {
                if (is_dir($conf)) {
                    $this->base = $conf;
                    include_once("{$this->base}/main.config.php");
                } else if (file_exists($conf)) {
                    include_once($conf);
                } else {
                    throw new Exception("could not load config {$conf}");
                }

                foreach ($values as $k => $v) {
                    $this->{$k} = $v;
                }

                $this->setup();
                return($this);
            } else {
                throw new Exception('config not defined');
            }
        }

        public function get($value=null, $key=null) {
            if (func_num_args() == 2) {
                return($this->{$value}[$key]);
            } else {
                return($this->{$value});
            }
        }

        private function setup() {
            foreach ($this->setup['base'] as $class) {
                if (!include_once("{$this->path['lib']}/base.{$class}.php")) {
                    throw new Exception("could not load base.{$class}.php");
                }
            }

            foreach ($this->setup['default'] as $name) {
                if (!include_once("{$this->path['lib']}/{$name}.class.php")) {
                    throw new Exception("could not load {$name}.class.php");
                }
            }
        }
    }
?>