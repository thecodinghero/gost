<?php
    class Processor extends Display {
        protected $db;


        public function __construct(&$state=null, &$db=null) {
            if (!empty($state)) {
                $this->state = &$state;
                $this->db    = &$db;
                $this->state->set('format');
                return($this);
            } else {
                throw new Exception('missing state in processor object');
            }
        }

        public function run() {}
    }
?>