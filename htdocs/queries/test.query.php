<?php
    class TestQuery extends Query {
        public function getMysqlUsers($rows=false) {
            $limit = $rows ? "LIMIT {$rows}" : null ;
            $q = ("
                SELECT User, Host
                FROM   user
                WHERE  1
                {$limit}
            ");
            return($this->query($this->prepare($q, $rows)));
        }
    }
?>