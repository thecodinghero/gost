<?php
    $values = array(
        'setup' => array(
            'base' => array(
                'object',
                'display',
                'module',
                'processor',
                'query'
            ),

            'default' => array(
                'db'
            )
        ),

        'path' => array(
            'lib'        => './base',
            'displays'   => './displays',
            'modules'    => './modules',
            'objects'    => './objects',
            'processors' => './processors',
            'queries'    => './queries',
            'templates'  => './templates',
            'tiles'      => './tiles'
        ),

        'db' => array(
            'host' => 'localhost',
            'user' => 'root',
            'pass' => 'root',
            'name' => 'mysql',
            'port' => 3306,
            'sock' => false
        ),

        'date'              => date('Y-m-d H:i:s'),
        'log_db_connection' => false,
        'debug_style'       => 'background-color: #ffff99; font-size: 8pt; clear: both; text-align: left;',

        'title'             => 'The G.O.S.T. Framework for PHP'
    );
?>